# Cockroach Serverless With Next.js

📂 Project files for an article on ThePracticalDev about building out a serverless application with CockroachDB, Prisma, and Next.js

Check out the [blog post](https://dev.to/harshhhdev/building-a-serverless-application-with-nextjs-and-cockroachdb-287m)!

# 🚀 Quickstart

Run the website locally

```
git clone https://gitlab.com/harshhh-dev/cockroach-serverless-with-next.js.git
```

## Setting up the project

```bash
cd cockroach-serverless-with-next.js

# Install deps
yarn
```

Rename `.env.local.EXAMPLE` to `.env.local`

Now, go to [CockroachDB], and create a new cluster. Create a user, and hit "connect", navigate over to connection string follow the instructions. 

Next, run `yarn prisma generate && yarn prisma db pull` to pull your schema into your `prisma/schema.prisma` file.

For a more in-depth explaination, be sure to checkout the [blog post](https://dev.to/harshhhdev/building-a-serverless-application-with-nextjs-and-cockroachdb-287m).

## Starting server

```bash
yarn dev
```

Server should now be running on [localhost](https://localhost:3000)

[CockroachDB]: https://cockroachlabs.com
