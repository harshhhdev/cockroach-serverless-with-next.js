import type { NextPage } from 'next'

import styles from '../styles/Home.module.css'

import SEO from '../components/SEO'
import Nav from '../components/Nav'
import Form from '../components/Form'

const New: NextPage = () => {
  return (
    <div className={styles.container}>
      <SEO />
      <div className={styles.box}>
        <Nav />
        <Form />
      </div>
    </div>
  )
}

export default New
