import type { GetServerSideProps, NextPage } from 'next'

import styles from '../styles/Home.module.css'

import SEO from '../components/SEO'
import Nav from '../components/Nav'
import Post from '../components/Post'

import prisma from '../lib/prisma'
import { post as PostType } from '@prisma/client'

const Home: NextPage<{ posts: PostType[] }> = ({ posts }) => {
  return (
    <div className={styles.container}>
      <SEO />
      <div className={styles.box}>
        <Nav />
        <Post posts={posts} />
      </div>
    </div>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const posts = await prisma.post.findMany()

  return {
    props: {
      posts,
    },
  }
}

export default Home
