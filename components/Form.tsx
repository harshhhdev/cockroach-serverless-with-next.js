import { FC, FormEvent, useRef } from 'react'
import { useRouter } from 'next/router'
import styles from '../styles/New.module.css'
import dompurify from 'dompurify'

const Form: FC = () => {
  const router = useRouter()

  const titleRef = useRef<HTMLInputElement>(null)
  const linkRef = useRef<HTMLInputElement>(null)
  const contentRef = useRef<HTMLTextAreaElement>(null)

  const createPost = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()

    const headers = new Headers()
    headers.append('Content-Type', 'application/json')

    const raw = JSON.stringify({
      title: dompurify.sanitize(titleRef.current?.value!),
      link: dompurify.sanitize(linkRef.current?.value!),
      content: dompurify.sanitize(contentRef.current?.value!),
    })

    const requestOptions: RequestInit = {
      method: 'POST',
      headers: headers,
      body: raw,
    }

    fetch('/api/new', requestOptions)
      .then((response) => response.json())
      .then((result) => router.push(`/post/${result.slug}`))
      .catch((error) => console.log('error', error))
  }

  return (
    <form className={styles.form} onSubmit={(e) => createPost(e)}>
      <input
        placeholder='Post title...'
        className={styles.input}
        type='text'
        ref={titleRef}
      />
      <input
        placeholder='Song link...'
        className={styles.input}
        type='url'
        ref={linkRef}
      />
      <textarea
        placeholder='Why you love this song...'
        className={styles.content}
        ref={contentRef}
      ></textarea>
      <button className={styles.create} type='submit'>
        Publish Post
      </button>
    </form>
  )
}

export default Form
