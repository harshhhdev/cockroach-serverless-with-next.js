import { FC } from 'react'
import Link from 'next/link'
import styles from '../styles/Home.module.css'

const Nav: FC = () => {
  return (
    <nav className={styles.nav}>
      <Link href='/'>
        <h1>Share Music 🎷</h1>
      </Link>
      <Link href='/new'>
        <a className={styles.create}>New Post</a>
      </Link>
    </nav>
  )
}

export default Nav
