import { FC } from 'react'
import styles from '../styles/Home.module.css'
import { post as PostType } from '@prisma/client'

const Post: FC<{ posts: PostType[] }> = ({ posts }) => {
  return (
    <>
      {posts.map((post, i) => (
        <div className={styles.post} key={i}>
          <a href={`/post/${post.id}`} className={styles.title}>
            {post.title}
          </a>
          <p className={styles.content}>
            {post.content.length > 200
              ? post.content.substring(0, 200) + '...'
              : post.content}
          </p>
          <a
            href={post.link}
            target='_blank'
            rel='noreferrer'
            className={styles.link}
          >
            {post.link}
          </a>
        </div>
      ))}
    </>
  )
}

export default Post
